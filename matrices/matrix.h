namespace WorkingWithMatrix
{
    using key = long long(*)(int*, int);

    int** createMatrix(int, int);
    int** createMatrix(int);
    void displayMatrixHeightWidth(int**, int, int, int);
    int** lowTriangleMatrix(int);
    void displayMatrix(int**, int);
    int** convertToHighTriangleMatrix(int**, int);
    int** convertTo(int*, int, int);
    int* convertTo(int**, int, int);
    int** allocateMemory(int rows, int columns);
    void sortMatrix(int**, int, int, key);
    void initMatrix(int**, int, int);
    void displayMatrix(int**, int, int, int);
    void swap(int*&, int*&);
    long long sumOfElements(int*, int);
    int* create(int**, int, int);
    void swap(long long&, long long&);
    long long productOfElements(int*, int);
}


