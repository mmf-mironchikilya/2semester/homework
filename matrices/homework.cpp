#include <iostream>
#include "matrix.h"
#include "array.h"

void fillMatrixRandom(int** matrix, int size, int minValue, int maxValue);
int** addMatrices(int** matrix1, int** matrix2, int rows, int columns);

int main() {
    int width = 5;
    int rows = 5;
    int columns = 5;
    int** matrix;

    matrix = WorkingWithMatrix::createMatrix(rows, columns);

    fillMatrixRandom(matrix, width, 1, 10);

    std::cout << "matrix 1" << std::endl;
    WorkingWithMatrix::displayMatrix(matrix, rows, columns, width);

    int** matrix2 = WorkingWithMatrix::createMatrix(rows, columns);
    fillMatrixRandom(matrix2, width, 1, 10);
    int** sumMatrix = addMatrices(matrix, matrix2, rows, columns);
    std::cout << "matrix 2" << std::endl;
    WorkingWithMatrix::displayMatrix(matrix2, rows, columns, width);
    std::cout << "Sum of matrices:" << std::endl;
    WorkingWithMatrix::displayMatrix(sumMatrix, rows, columns, width);

    return 0;
}

void fillMatrixRandom(int** matrix, int size, int minValue, int maxValue) {
    if(matrix == nullptr){
        throw std::invalid_argument("Nullptr pointer is invalid");
    }

    if (size < 0){
        throw std::out_of_range("Array length is invalid");
    }

    for (int i = 0; i < size; i++) {
        for (int j = i; j < size; j++) {
//            if (i == j){
//                continue;
//            }
            int value = rand() % (maxValue - minValue + 1) + minValue;
            matrix[i][j] = value;
            matrix[j][i] = value;
        }
    }
}

int** addMatrices(int** matrix1, int** matrix2, int rows, int columns) {
    if(matrix1 == nullptr && matrix2 == nullptr){
        throw std::invalid_argument("Nullptr pointer is invalid");
    }

    if (rows < 0 && columns < 0){
        throw std::out_of_range("Array length is invalid");
    }

    int** result = WorkingWithMatrix::createMatrix(rows, columns);

    for (int i = 0; i < rows; ++i) {
        for (int j = i; j < columns; ++j) {
            result[i][j] = matrix1[i][j] + matrix2[i][j];
            result[j][i] = result[i][j];
        }
    }

    return result;
}
