#include <iostream>
#include "matrix.h"
#include "array.h"

using namespace std;

int** WorkingWithMatrix::createMatrix(int size) {
    if (size < 0){
        throw std::out_of_range("Array length is invalid");
    }

    int** matrix = new int*[size];
    for (int i = 0; i < size; ++i) {
        matrix[i] = new int[size];
    }
    return matrix;
}

int** WorkingWithMatrix::createMatrix(int n, int m){
    int** matrix = new int* [n];
    for (size_t i = 0; i < n; i++) {
        matrix[i] = new int [m];
    }
    for (size_t i = 0; i < n; i++) {
        WorkingWithArrays::allocateMemory(n);
        //WorkingWithArrays::randomValues(matrix[i], m);
    }
    return matrix;
}

void WorkingWithMatrix::displayMatrixHeightWidth(int** matrix, int n, int m, int width) {
    if(matrix == nullptr){
        throw std::invalid_argument("Nullptr pointer");
    }

    if (n < 0 && m < 0){
        throw std::out_of_range("Enter valid array sizes");
    }

    for (size_t i = 0; i < n; i++) {
        for (size_t j = 0; j < m; j++) {
            std::cout.width(width);
            std::cout << matrix[i][j];
        }
        std::cout << std::endl;
    }
}

int** WorkingWithMatrix::lowTriangleMatrix(int size){
    if (size < 0){
        throw std::out_of_range("Array length must be more than 0");
    }
    int** matrix = new int* [size];
    for (size_t i = 0; i < size; i++) {
        int k = k + 1;
        WorkingWithArrays::allocateMemory(k);
        WorkingWithArrays::randomValues(matrix[i], k);
     }
     return matrix;
}

void WorkingWithMatrix::displayMatrix(int** matrix, int size){
    if(matrix == nullptr){
        throw std::invalid_argument("Nullptr pointer");
    }

    if (size < 0){
        throw std::out_of_range("Array length");
    }

    for (size_t i = 0; i < size; i++) {

        for (size_t j = 0; j < size; j++) {
            std::cout.width(5);
            std::cout << 0 << " ";
        }
        WorkingWithArrays::displayArray(matrix[i], i+1);
        std::cout << std::endl;
    }
}

int** WorkingWithMatrix::convertToHighTriangleMatrix(int** matrix, int size){
    if(matrix == nullptr){
        throw std::invalid_argument("Nullptr pointer");
    }

    if (size < 0){
        throw std::out_of_range("Array length must be more than 0");
    }

    int** copy = new int* [size];
    for (size_t i = 0; i < size; i++) {
        copy[i] = WorkingWithArrays::allocateMemory(size - i);
        for (size_t j = 0; j < size-i; j++) {
            copy[i][j] = matrix[j+i][i];
        }
    }
    return copy;
}

int** WorkingWithMatrix::convertTo(int* array, int rows, int columns){
    if (rows <= 0 && columns <= 0){
        throw std::out_of_range("Array length must be more than 0");
    }
    if(array == nullptr){
        throw std::invalid_argument("Nullptr pointer");
    }

    int** matrix = WorkingWithMatrix::allocateMemory(rows, columns);
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            matrix[i][j] = array[columns * i + j];
        }
    }
}

int* WorkingWithMatrix::convertTo(int** matrix, int rows, int columns) {
    if (rows <= 0 && columns <= 0){
        throw std::out_of_range("Array length must be more than 0");
    }
    if(matrix == nullptr){
        throw std::invalid_argument("Nullptr pointer");
    }
    int* array = WorkingWithArrays::allocateMemory(rows * columns);
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            array[columns * i + j] = matrix[i][j];
        }
    }
    return array;
}

int** WorkingWithMatrix::allocateMemory(int rows, int columns) {
    if (rows < 0 && columns < 0){
        throw std::out_of_range("Array length must be more than 0");
    }

    int** matrix = new int* [rows];
    for (size_t i = 0; i < rows; i++) {
        matrix[i] = WorkingWithArrays::allocateMemory(columns);
    }

    return matrix;
}

void WorkingWithMatrix::swap(int*& a, int*& b){
    int* temp = a;
    a = b;
    b = temp;
}

void WorkingWithMatrix::initMatrix(int** matrix, int rows, int columns){
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < columns; ++j) {
            cout << "[" << (i+1) << "," << (j+1) << "] = ";
            cin >> matrix[i][j];
        }
    }
}

void WorkingWithMatrix::displayMatrix(int** matrix, int rows, int columns, int width) {
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < columns; ++j) {
            cout.width(width);
            cout << matrix[i][j];
        }
        cout << endl;
    }
    cout << endl;
}

long long WorkingWithMatrix::sumOfElements(int* array, int size){
    long long sum = 0;
    for (int i = 0; i < size; ++i) {
        sum += array[i];
    }
    return sum;
}

long long WorkingWithMatrix::productOfElements(int* array, int size){
    long long sum = 1;
    for (int i = 0; i < size; ++i) {
        sum *= array[i];
    }
    return sum;
}

void WorkingWithMatrix::sortMatrix(int** matrix, int rows, int columns,key func){
    for (int i = 0; i < rows; i++) {
        for (int j = rows - 1; j > i; j--) {
            if (func(matrix[j], columns) < func(matrix[j-1], columns)) {
                swap(matrix[j], matrix[j-1]);
            }
        }
    }
}