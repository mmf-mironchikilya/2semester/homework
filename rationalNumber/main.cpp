#include <iostream>
#include "rational.h"

using namespace std;

int main() {
    Rational a(1, 2);
    Rational b(3, 4);

    Rational c = a + b;
    c.print();

    Rational d = a - b;
    d.print();

    Rational e = a * b;
    e.print();

    Rational f = a / b;
    f.print();

    return 0;
}
