#ifndef CLASSES_RATIONAL_H
#define CLASSES_RATIONAL_H

class Rational{
private:
    int numerator;
    int denominator;

public:
    explicit Rational(int numerator = 0, int denominator = 1){
        if (denominator == 0)
        {
            throw std::invalid_argument("Not allowed argument");
        }

        this->numerator = numerator;
        this->denominator = denominator;
    }

    Rational operator+(const Rational& other) const{
        return Rational(numerator * other.denominator + other.numerator * denominator,denominator * other.denominator);
    }

    Rational operator-(const Rational& other) const{
        return Rational(numerator * other.denominator - other.numerator * denominator,denominator * other.denominator);
    }

    Rational operator*(const Rational& other) const{
        return Rational(numerator * other.numerator, denominator * other.denominator);
    }

    Rational operator/(const Rational& other) const{
        return Rational(numerator * other.denominator, denominator * other.numerator);
    }

    void print() const{
        std::cout << numerator << "/" << denominator << std::endl;
    }
};

#endif //CLASSES_RATIONAL_H
