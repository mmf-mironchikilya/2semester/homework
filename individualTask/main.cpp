#include <iostream>
#include <algorithm>

using namespace std;

int countOnes(int number);
bool compare(int a, int b);
int findMin(int arr[], int size);
int* findFinalArray(int* arr, int size, int* finalArr, int arrSize, int minElement);
void printArray(int* arr, int size);

int main() {
    int arr[] = {5, 3, 10, 15, 20, 25, 30, 21};
    int size = sizeof(arr) / sizeof(arr[0]);
    int result[100];
    int resultSize = 0;

    int minElement = findMin(arr, size);
    findFinalArray(arr, size, result, resultSize, minElement);

    std::cout << "Отсортированный массив: ";
    printArray(result, resultSize);
}

int findMin(int arr[], int size){
    int result = arr[0];
    for (int i = 1; i < size; ++i) {
        if (arr[i] < result) {
            result = arr[i];
        }
    }
    return result;
}

int* findFinalArray(int* arr, int size, int* finalArr, int arrSize, int minElement){
    cout << size << endl;
    int j = 0;
    for (int i = 0; i < size; ++i) {
        if (arr[i] % minElement == 0) {
            finalArr[j] = arr[i + 1];
            j++;
        }
    }
    return finalArr;
}

void printArray(int* arr, int size){
    for (int i = 0; i < size; ++i) {
        cout << arr[i] << " ";
    }
    cout << endl;
}
